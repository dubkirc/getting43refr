package ru.dubki.base;

/**
 * 
 * Powered by GulamsounovTA (c) 2011-2014
 * 
 **/

import java.math.BigDecimal;

public class Functions {

	public final static String piece(String str,String delimiter,Integer... fields){
		if (str==null || fields.length==0) return "";
		String[] tmpArray = str.split(delimiter);
			
		if (fields.length==1){
			if (fields[0] < 1 || fields[0] > tmpArray.length) return "";
			return tmpArray[fields[0] - 1];
		}
			
		if (fields.length==2){
			String dl = delimiter.replaceAll("[\\\\]","");
			StringBuffer buf = new StringBuffer("");
			for (int i = fields[0];i <= fields[1];i++){
				if (i < 0 || i > tmpArray.length) return buf.length()!=0?buf.substring(0, buf.length()-dl.length()):buf.toString();
				buf.append(tmpArray[i-1]);
				buf.append(dl);
			}
			return buf.length()!=0?buf.substring(0, buf.length()-dl.length()):buf.toString();
		}
			
		return "";
	}
	
	public final static float round(float val, int numRound){
		BigDecimal x = new BigDecimal(val);
		x = x.setScale(numRound, BigDecimal.ROUND_HALF_UP);
		return x.floatValue();
	}
}
