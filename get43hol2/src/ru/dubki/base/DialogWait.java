package ru.dubki.base;

/**
 * 
 * Powered by GulamsounovTA (c) 2011-2014
 * 
 **/

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.InputType;
import android.widget.DatePicker;
import android.widget.EditText;
import static ru.dubki.base.Functions.piece;

public class DialogWait {
	
	private Context mContext;
	private Handler mHandler;
	private ProgressDialog mProgress;
	private boolean mSemafor;

	private static enum TypeDialog {
		DialogProgress, DialogProgressH, DialogOk, DialogList, DialogQuest, DialogInput, DialogDate,
		DialogCheck
	}
	
	private static class ExtDialog {
		private String mTitle;
		private String mMsg;
		private TypeDialog mType;
		private Object mContent;
		private boolean mWait;
		private OnClickListener mOnClickPositive;
		private OnClickListener mOnClickNegative;
		private OnDateSetListener mOnDateSetListener;
		
		public ExtDialog(String title, String msg, TypeDialog type) {
			mTitle = title;
			mMsg = msg;
			mType = type;
			mWait = true;
		}
		
		public void setDateButton(OnDateSetListener onDateSetListener) {
			mOnDateSetListener = onDateSetListener;
		}
		
		public void setPositiveButton(OnClickListener onClickPositive) {
			mOnClickPositive = onClickPositive;
		}
		
		public void setNegativeButton(OnClickListener onClickNegative) {
			mOnClickNegative = onClickNegative;
		}
	}

	public void setProgress(int value){
		if (mProgress != null) mProgress.setProgress(value);		
	}

	public DialogWait(Context context) {
		mContext = context;	
		mHandler = new Handler(mCallback);
	}

	public void hideDialog(){
		if (mProgress != null) {
			mProgress.dismiss();
			mProgress = null;
		}
	}

	private Handler.Callback mCallback = new Handler.Callback() {
		
		@Override
		public boolean handleMessage(Message arg0) {	
			hideDialog();
			
			ExtDialog extDialog = (ExtDialog) arg0.obj;
			switch (extDialog.mType) {
			case DialogProgress:
				ProgressDialog pd = new ProgressDialog(mContext);
				pd.setTitle(extDialog.mTitle);
				pd.setMessage(extDialog.mMsg);
				pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
				pd.setCancelable(false);
				pd.show();
				mProgress = pd;
				break;	
			case DialogProgressH:
				ProgressDialog pd1 = new ProgressDialog(mContext);
				pd1.setTitle(extDialog.mTitle);
				pd1.setMessage(extDialog.mMsg);
				pd1.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
				pd1.setMax(100);
				pd1.setProgress(0);
				pd1.setCancelable(false);
				pd1.show();
				mProgress = pd1;
				break;					
			case DialogOk:	
				AlertDialog.Builder dialogOk = new AlertDialog.Builder(mContext);
				dialogOk.setCancelable(false);
				dialogOk.setTitle(extDialog.mTitle);
				dialogOk.setMessage(extDialog.mMsg);
				dialogOk.setPositiveButton("OK", extDialog.mOnClickPositive);
				dialogOk.setCancelable(false);
				dialogOk.show();	
				break;
			case DialogQuest:
				AlertDialog.Builder dialogQuest = new AlertDialog.Builder(mContext);
				dialogQuest.setCancelable(false);
				dialogQuest.setTitle(extDialog.mTitle);
				dialogQuest.setMessage(extDialog.mMsg);
				dialogQuest.setPositiveButton("��", extDialog.mOnClickPositive);
				dialogQuest.setNegativeButton("���", extDialog.mOnClickNegative);
				dialogQuest.setCancelable(false);
				dialogQuest.show();
				break;
			case DialogList:
				AlertDialog.Builder dialogList = new AlertDialog.Builder(mContext);
				dialogList.setTitle(extDialog.mTitle);
				dialogList.setCancelable(false);
				dialogList.setItems((String [])extDialog.mContent, extDialog.mOnClickPositive);
				dialogList.setNegativeButton("������", extDialog.mOnClickNegative);
				dialogList.show();
				break;
			case DialogCheck:
				String [] data = (String [])extDialog.mContent;
				final boolean [] result = new boolean[data.length];
				extDialog.mContent = result;
				AlertDialog.Builder dialogCheckList = new AlertDialog.Builder(mContext);
				dialogCheckList.setTitle(extDialog.mTitle);
				dialogCheckList.setMultiChoiceItems(data, result, new DialogInterface.OnMultiChoiceClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which, boolean isChecked) {
						result[which] = isChecked;
					}
				});
				dialogCheckList.setCancelable(false);
				dialogCheckList.setPositiveButton("OK", extDialog.mOnClickPositive);
				dialogCheckList.setNegativeButton("������", extDialog.mOnClickNegative);
				dialogCheckList.show();
				break;
			case DialogInput:
				EditText edit = new EditText(mContext);
				edit.setInputType(InputType.TYPE_CLASS_TEXT);
				edit.setFocusable(true);	
				extDialog.mContent = edit;
				AlertDialog.Builder dialogInput = new AlertDialog.Builder(mContext);
				dialogInput.setView((EditText)extDialog.mContent);
				dialogInput.setCancelable(false);
				dialogInput.setTitle(extDialog.mTitle);
				dialogInput.setMessage(extDialog.mMsg);
				dialogInput.setPositiveButton("OK", extDialog.mOnClickPositive);
				dialogInput.show();
				break;
			case DialogDate:
				Calendar calendar = Calendar.getInstance();
				new DatePickerDialog(mContext, extDialog.mOnDateSetListener, 
					calendar.get(Calendar.YEAR), 
					calendar.get(Calendar.MONTH), 
					calendar.get(Calendar.DAY_OF_MONTH))
				.show();
				break;
			default:
				break;
			}		
			mSemafor = false;
			return true;
		}
	};
	
	private void showDialogWait(ExtDialog extDialog) {
		if (Looper.getMainLooper().getThread() == Thread.currentThread()) {
			mCallback.handleMessage(mHandler.obtainMessage(0, extDialog));
		} else {
			mSemafor = true;
			mHandler.sendMessage(mHandler.obtainMessage(0, extDialog));
			while(mSemafor);
			if (extDialog.mType == TypeDialog.DialogProgress || extDialog.mType == TypeDialog.DialogProgressH) return;
			synchronized (extDialog) {
				try {
					while(extDialog.mWait) extDialog.wait();				
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public void showDialogProgress(String title, String message) {				
		ExtDialog extDialog = new ExtDialog(title, message, TypeDialog.DialogProgress);
		showDialogWait(extDialog);
	}
	
	public void showDialogProgressH(String title,String message) {		
		ExtDialog extDialog = new ExtDialog(title, message, TypeDialog.DialogProgressH);
		showDialogWait(extDialog);
	}
	
	public void showDialogBox(String title, String message) {
		final ExtDialog extDialog = new ExtDialog(title, message, TypeDialog.DialogOk);
		extDialog.setPositiveButton(new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				synchronized (extDialog) {
					extDialog.mWait = false;
					extDialog.notifyAll();
				}	
				
			}
		});	
		
		showDialogWait(extDialog);
	}
	
	public void showDialogQuest(String title, String message, OnClickListener onClickListener) {
		ExtDialog extDialog = new ExtDialog(title, message, TypeDialog.DialogQuest);
		extDialog.setPositiveButton(onClickListener);
		extDialog.setNegativeButton(onClickListener);
		showDialogWait(extDialog);
	}
	
	public boolean showDialogQuest(String title, String message) {
		final ExtDialog extDialog = new ExtDialog(title, message, TypeDialog.DialogQuest);
		extDialog.setPositiveButton(new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				synchronized (extDialog) {
					extDialog.mContent = true;
					extDialog.mWait = false;
					extDialog.notifyAll();					
				}	
				
			}
		});	
		
		extDialog.setNegativeButton(new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				synchronized (extDialog) {
					extDialog.mContent = false;
					extDialog.mWait = false;
					extDialog.notifyAll();					
				}					
			}
		});	
	
		showDialogWait(extDialog);
		return (Boolean) extDialog.mContent;
	}
	
	public String showDialogDate(String title, String message) {
		final StringBuilder builder = new StringBuilder();
		final ExtDialog extDialog = new ExtDialog(title, message, TypeDialog.DialogDate);
		extDialog.setDateButton(new OnDateSetListener() {
			
			@Override
			public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
				synchronized (extDialog) {
					Calendar calendar = new GregorianCalendar();
					calendar.set(year, monthOfYear, dayOfMonth);
					SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy",Locale.getDefault());
					builder.append(sdf.format(calendar.getTime()));
					extDialog.mWait = false;			
					extDialog.notifyAll();	
				}							
			}
		});
	
		showDialogWait(extDialog);		
		return builder.toString();
	}
	
	public String showDialogInput(String title, String message) {
				
		final StringBuilder builder = new StringBuilder();
		final ExtDialog extDialog = new ExtDialog(title, message, TypeDialog.DialogInput);
		extDialog.setPositiveButton(new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				synchronized (extDialog) {
					builder.append(((EditText)extDialog.mContent).getText().toString());
					extDialog.mWait = false;			
					extDialog.notifyAll();	
				}				
			}
		});

		showDialogWait(extDialog);
		return builder.toString();
	}
	
	public String showDialogList(String title, final ArrayList<String> msg, String delimiter, int position){
		final StringBuilder builder = new StringBuilder();
		
		ArrayList<String> list = new ArrayList<String>();
		for(String item : msg) {
			list.add(piece(item, delimiter, position));
		}
		String [] array = list.toArray(new String[list.size()]);
				
		final ExtDialog extDialog = new ExtDialog(title, null, TypeDialog.DialogList);
		extDialog.mContent = array;
		extDialog.setPositiveButton(new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				synchronized (extDialog) {
					builder.append(msg.get(arg1));
					extDialog.mWait = false;			
					extDialog.notifyAll();	
				}				
			}
		});
		
		extDialog.setNegativeButton(new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				synchronized (extDialog) {
					extDialog.mWait = false;			
					extDialog.notifyAll();
				}
			}
		});
		
		showDialogWait(extDialog);
		return builder.toString();	
	}
	
	public boolean [] showDialogCheckList(String title, final ArrayList<String> msg, String delimiter, int position) {
		
		
		ArrayList<String> list = new ArrayList<String>();
		for(String item : msg) {
			list.add(piece(item, delimiter, position));
		}
		String [] array = list.toArray(new String[list.size()]);
	
		final ExtDialog extDialog = new ExtDialog(title, null, TypeDialog.DialogCheck);
		extDialog.mContent = array;
		extDialog.setPositiveButton(new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				synchronized (extDialog) {					
					extDialog.mWait = false;			
					extDialog.notifyAll();	
				}				
			}
		});
		
		extDialog.setNegativeButton(new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				synchronized (extDialog) {
					extDialog.mWait = false;			
					extDialog.notifyAll();
				}
			}
		});
		
		showDialogWait(extDialog);
		return (boolean [])extDialog.mContent;
	}
	
	/*
	
	public boolean[] showDialogCheckList(String title,final ArrayList<String> msg,String delimiter,int position){
		final State state = new State();
		
		String [] mArray;
		if (position>0){
			ArrayList<String> mTmp = new ArrayList<String>();
			for(String element:msg){
				mTmp.add(piece(element,delimiter,position));
			}
			mArray = mTmp.toArray(new String[mTmp.size()]);
		}else{
			mArray = msg.toArray(new String[msg.size()]);
		}
		
		final boolean [] mResultMulti = new boolean[mArray.length];
		
		AlertDialog.Builder dialog = new AlertDialog.Builder(mContext);
		dialog.setTitle(title);
		dialog.setMultiChoiceItems(mArray,mResultMulti,new DialogInterface.OnMultiChoiceClickListener() {
			public void onClick(DialogInterface dialog, int which, boolean isChecked) {
				mResultMulti[which] = isChecked;
			}
		});
		dialog.setCancelable(false);
		dialog.setNegativeButton("������", new OnClickListener() {
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				synchronized (state) {
					state.setState(false);
					state.notifyAll();
				}	
			}
		});
		
		dialog.setPositiveButton("������", new OnClickListener() {
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				synchronized (state) {
					state.setState(false);
					state.notifyAll();
				}	
			}
		});
		
		showDialogWait(new MDialog(dialog, state));
		return mResultMulti;
	}
	
	public String showDialogList(String title,final ArrayList<String> msg,String delimiter,int position){
		final State state = new State();
		final StringBuilder builder = new StringBuilder();
		
		String [] mArray;
		if (position>0){
			ArrayList<String> mTmp = new ArrayList<String>();
			for(String element:msg){
				mTmp.add(piece(element,delimiter,position));
			}
			mArray = mTmp.toArray(new String[mTmp.size()]);
		}else{
			mArray = msg.toArray(new String[msg.size()]);
		}
		
		AlertDialog.Builder dialog = new AlertDialog.Builder(mContext);
		dialog.setTitle(title);
		dialog.setCancelable(false);
		dialog.setNegativeButton("������", new OnClickListener() {
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				synchronized (state) {
					state.setState(false);
					state.notifyAll();
				}	
			}
		});
		dialog.setItems(mArray, new OnClickListener() {
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				synchronized (state) {
					builder.append(msg.get(arg1));
					state.setState(false);
					state.notifyAll();
				}
			}
		});
		showDialogWait(new MDialog(dialog, state));
		return builder.toString();
	}
	
	public String showDialogInput(String title,String message){
		final State state = new State();
		final EditText edit = new EditText(mContext);
		edit.setInputType(InputType.TYPE_CLASS_TEXT);
		edit.setFocusable(true);
		final StringBuilder builder = new StringBuilder();
		AlertDialog.Builder dialog = new AlertDialog.Builder(mContext);
		dialog.setView(edit);
		dialog.setCancelable(false);
		dialog.setTitle(title);
		dialog.setMessage(message);
		dialog.setPositiveButton("OK",new OnClickListener() {
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				synchronized (state) {
					builder.append(edit.getText().toString());
					state.setState(false);
					state.notifyAll();
				}				
			}
		});
		showDialogWait(new MDialog(dialog, state));
		return builder.toString();
	}
	
	
	}
	*/
}
