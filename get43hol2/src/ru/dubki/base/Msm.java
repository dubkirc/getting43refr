package ru.dubki.base;

/**
 * 
 * Powered by GulamsounovTA (c) 2011-2014
 * 
 **/

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Enumeration;

import org.apache.http.util.EncodingUtils;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;

import java.util.zip.*;

import static android.os.Environment.getExternalStorageDirectory;
import static ru.dubki.base.Functions.piece;

public class Msm {
	
	/** �������������� **/
	public static final int UPDATE_ERROR = -1;
	public static final int UPDATE_YES = 0;
	public static final int UPDATE_NO = 1;
	public static final int FILE_DOWNLOAD = 1;
	
	/** ������� **/
	public ArrayList<String> outArray;
	public String outRet;
	public String mError;
	private Socket mSocket;
	private String mUCI;
	private String mVOL;
	private String mEncoding;
    private String mIp;
    private String mPort;
    private String mTime;
    private Intent mIntent;
     
    public static final int CHECK_MODE = 0;
    public static final int IGNORE_MODE = 1;
    public static final int STRICT_MODE = 2;
    	
    public void setUciVol(String Uci,String Vol){
    	mVOL = Vol;
    	mUCI = Uci;
    }
    
    /** ����������� **/
    public Msm() {
    	outArray = new ArrayList<String>();
    	outRet = "";
    	mTime = "";
    	mPort = "";
    	mIp = "";
    	mVOL = "";
    	mUCI = "";
    	mEncoding = "Cp1251";
	}
    
    public boolean connect(String param){
    	String[] tmpArray = param.split("\\|");
    	if (tmpArray.length < 4) {
    		mError = "�� ��������� ������ ��������� �����������";
    		return false;
    	}
    	
    	String ip = tmpArray[0];
		String port = tmpArray[1];
		String uci = tmpArray[2];
		String vol = tmpArray[3];
		String timeOut = ip.contains("192.168")?"3":"15";
    	return connect(ip, port, timeOut, uci, vol);
    }
    
    /** ������������ � �������
     * @param server - ip �����
     * @param port - ����
     * @param iddleTime - ����� ��������
     * @param uci - ���
     * @param vol - ���
     */
	public Boolean connect(String server,String port,String iddleTime,String uci,String vol)
	{
		mIp = server;
		mPort = port;
		mTime = iddleTime;
		mUCI = uci;
		mVOL = vol;
		
		if (server.equals("") || port.equals("") || iddleTime.equals("") || uci.equals("") || vol.equals("")){
			mError = "�� ��������� ������ ��������� �����������";
			return false;
		}
			
		try{
			mSocket = new Socket();
			mSocket.setSoTimeout(Integer.valueOf(iddleTime)*1000);
			mSocket.connect(new InetSocketAddress(server,Integer.valueOf(port)),Integer.valueOf(iddleTime)*1000);
			passParToMSM("ITIME",iddleTime.toString());
		}catch (Exception e) {
			mError = e.getMessage();
			if (mError == null) mError = "�� ������� ������������ � �������. ���������� ��� ���.";
			disconnect();
			return false;
		}
		
		return true;
	}
	
    /** ��������� ��������� �� ������� MSM
     * @param routine - ��� ���������
     * @param par - ��������� ���������
     */
    public boolean run(String routine,Object... par) {
		
    	// ���� ����������� ���, ��� ����� ����������
		if (mSocket==null && !connect(mIp, mPort, mTime, mUCI, mVOL)){
			return false;
		}
		
		// ������� outarr �������
		outArray.clear();
		outRet="";
		StringBuilder resultBuffer = new StringBuilder();
		byte [] buffer = new byte[1024 * 8];
		
		try{
			
			// ��������� �������
			passParToMSM("UCI", mUCI);
			passParToMSM("VOL", mVOL);
			passParToMSM("ENCODE", "1");
			passParToMSM("DELINARR", "1");
			passParToMSM("ROUT", routine);
			
			// �������� ���������� 	
			int count = 1;
			for (Object item : par){
				if (item == null) item = "";
				passParToMSM(count, item);
				count++;
			}
			
			// ��������� ��������� 
			passParToMSM("RUN", "1");
			
			// ��������� ����������
			int size = 0;
			boolean flReadSocket = true;
			do{
				size = mSocket.getInputStream().read(buffer);
				if (size < 0) throw new IOException("���������� ������� ��������. ���������� ��� ���.");
				resultBuffer.append(new String(buffer, 0, size, mEncoding));
				if (resultBuffer.toString().contains("~@ERR:")) throw new IOException(resultBuffer.toString());
				if (resultBuffer.toString().endsWith("~@OUT@END@~")) flReadSocket = false;
			}while(flReadSocket);
	
		}catch (Exception e) {
			mError = e.getMessage();
			if (mError == null) mError = "������ �������� ������. ���������� ��� ���.";
			return false;
		}finally{
			buffer = null;
			disconnect();
		}
		
		// ����� ���������� ������
		String[] array1level = resultBuffer.toString().split("~@OUT@");
		for(String item : array1level){
			if (item.equals("") || item.equals("END@~")) continue;
			String[] array2level = item.split("@");	
			if (array2level.length < 2){
				mError = "������ ������� ������. ���������� � �������������.";
				return false;
			}
			
			String parNum = array2level[0]; 
			String indexArray = array2level[1];
			String data = piece(item, "@~", 2);
			
			if (parNum.equals("2") && !indexArray.equals("") && !data.equals("")){
				outArray.add(data);
				continue;
			}

			if (indexArray.equals("")) outRet = data;
		}
		
		return true;
	}
	
	/** ��������� ������ **/
	private void check() throws IOException {
		byte[] buffer = new byte[1024];
		int size = mSocket.getInputStream().read(buffer);
		if (size < 0) throw new IOException("������ �������� ������. ���������� ��� ���.");
		String result = new String(buffer,0,size,mEncoding);
		if (!result.contains("~@OK@~")) throw new IOException(result);
		buffer = null;
	}
	 
	/** ���������� ������ �� ������
	 * @param parNum - ����� ��������� ��� ��� ���������� 
	 * @param par - ������ ������ ��� �������� ����������
	 */
	@SuppressWarnings({ "unchecked" })
	private void passParToMSM(Object parNum,Object par) throws IOException {
		byte[] mBuffer = new byte[1024];
		
		if (parNum.getClass()==Integer.class){
			if (par.getClass()==ArrayList.class){
				int count = 0;
				for (String item : (ArrayList<String>)par)
				{
					mBuffer = EncodingUtils.getBytes("ARR" + (char)0 + parNum.toString() + (char)0 + count + (char)0 + item + (char)0,mEncoding);
					mSocket.getOutputStream().write(mBuffer);
					check();
					count += 1;
				}	
			}else{
				mBuffer = EncodingUtils.getBytes("STR" + (char)0 + parNum.toString() + (char)0 + (char)0 + par.toString() + (char)0,mEncoding);
				mSocket.getOutputStream().write(mBuffer);
				check();
			}
		}else{
			mBuffer = EncodingUtils.getBytes((char)0 + parNum.toString() + (char)0 + par.toString() + (char)0, mEncoding);
			mSocket.getOutputStream().write(mBuffer);
			if (!parNum.toString().equals("RUN")) check();
		}
		
		mBuffer = null;
	}

    public String getMessage() {
    	return mError;
    }
     
	/** ���������� �� ������� **/
	public void disconnect() {
		try{
			mSocket.setSoTimeout(1000);
			passParToMSM("QUIT","1");
			mSocket.setSoTimeout(Integer.valueOf(mTime)*1000);
		}catch(Exception e) { 
			// ������ �� ��� ����������
		}	
		
		try{
			mSocket.close();
		}catch (Exception e) { 
			// � �� ��� ����
		}
		mSocket = null;
	}
	
	public boolean downloadFile(Handler hnd,String pathSever,String pathLocal) {
		
		Log.d("TEST","Download file...");
		
		// �������� ����������
		if (mSocket==null && !connect(mIp, mPort, mTime, mUCI, mVOL)) {
			return false;
		}
		
		outRet = "";
	
		// ��������� �����
		File file = new File(pathLocal);
		if (!file.getParentFile().isDirectory()) file.getParentFile().mkdir();
		if (file.exists()) file.delete();
		
		try {
			file.createNewFile();
		} catch (IOException e) {
			mError = e.getMessage();
			if (mError == null) mError = "�� ������� ������� ����";
			return false;
		}
		
		// ��������� ����
		FileOutputStream os;
		try{
			os = new FileOutputStream(file);
		}catch (Exception e) {
			mError = e.getMessage();
			if (mError == null) mError = "�� ������� ������� ����";
			return false;
		}
			
		// ������������� ������ ������
		int size = 0, planSize = 0, factSize = 0, startIndex=0;
		byte [] tmpBuffer = new byte[1024 * 8];
		
		// ������
		try{
			mSocket.setSoTimeout(5000);
			passParToMSM("DFILE", pathSever);
			passParToMSM("RUN", "1");
			do{
				try{
					size = mSocket.getInputStream().read(tmpBuffer);
				}catch (Exception e) {
					break;
				}
				Log.d("TEST","Size recive: "+size);
				if (size <= 0) break;
				String result = new String(tmpBuffer, 0, size, mEncoding);
				if(result.contains("~@ERR:")) {	
					os.flush();
					os.close();
					throw new IOException(result);
				}
				startIndex = 0;
				if (piece(result, "@",2).equals("FILESIZE")) {
					planSize = Integer.valueOf(piece(result, "@",3));
					startIndex = result.indexOf("@~")+2;
				}
				if (planSize==0) {
					os.flush();
					os.close();
					throw new IOException("�� ������� ���������� ������ �����");
				}
				os.write(tmpBuffer, startIndex, size-startIndex);
				factSize +=size;  
				if (hnd !=null) hnd.sendMessage(hnd.obtainMessage(FILE_DOWNLOAD,planSize,factSize));
			}while(true);
			
			Log.d("TEST","Size p/f: "+planSize+" / "+factSize);
			
		}catch (Exception e) {
			mError = e.getMessage();
			if (mError == null) mError = "������ �������� ������. ���������� ��� ���.";
			return false;
		}finally{
			try {
				os.flush();
				os.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			disconnect();
		}
		
		if (factSize < planSize) {
			mError = "�� ������� ������� ����. ���������� ��� ���.";
			return false;
		}
		
		return true;
	}
	
	/** ����������� ����
	 * @param fileName - ����� � ����� �� ����������
	 * @return - true ���� ������� ����������� ����, ����� false
	 */
	public boolean unZip(File fileName){
		try {
			ZipFile zip = new ZipFile(fileName);
			Enumeration<? extends ZipEntry> zipEnum = zip.entries();
			ArrayList<ZipEntry> zipList = new ArrayList<ZipEntry>();
			while (zipEnum.hasMoreElements()) {
				ZipEntry entry = zipEnum.nextElement();
				if (entry.isDirectory()) {
					//new File(dir_to+"/"+entry.getName()).mkdir();
				} else {
					zipList.add(entry);
				}
			}
			for (ZipEntry entry : zipList) {
				new File(fileName.getParent()+"/"+entry.getName()).delete();
				InputStream in = zip.getInputStream(entry);
			    OutputStream out = new FileOutputStream(fileName.getParent()+"/"+entry.getName());
			    byte[] buffer = new byte[1024];
			    int len;
			    while ((len = in.read(buffer)) >= 0)
			    	out.write(buffer, 0, len);
			    
			    in.close();
			    out.close();
			}
			zip.close();
			fileName.delete();
		} catch (IOException e) {
			mError = e.getMessage();
			if (mError == null) mError = "Error uzip file";
			return false;
		}
		return true;
	}
	
	/** �������� ������������
	 * @param msmID - �������������� � ���� MSM
	 * @param context - ��������
	 * @return - ���������� -1 ��� ������������� ������, 1 - ������ ���������
	 */
	public int getUpdate(Handler Hnd,String msmID,Context context,String apkName,int mode){
		
		if (mSocket==null && !connect(mIp, mPort, mTime, mUCI, mVOL)){
			return UPDATE_ERROR;	
		}
		
		PackageInfo pInfo = null;
		try {
			pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
		} catch (NameNotFoundException e) {
			mError = "�� ������� ���������� ������ ������";
			return UPDATE_ERROR;
		}

		if(!run("VALFRMSM^DUSPR","^ANDROID(\""+msmID+"\",\"VERSION\")",null)){
			return UPDATE_ERROR;
		}
		
		String versionMsm = outRet.split("\\|")[0];
		if(versionMsm.equals("NOT FOUND")) return UPDATE_NO;
		
		String [] update = outRet.split("\\|");
		if (update.length<7){
			mError = "��������� ������ ���������� �� ����������";
			return UPDATE_ERROR;
		}
		
		// ���� ������������ ������
		int isVersionActual = UPDATE_YES;
		if (versionMsm.equals(pInfo.versionName)) isVersionActual = UPDATE_NO;
			
		// � ������ �������� ���������� ������������
		if (mode==CHECK_MODE) return isVersionActual;
				
		// � ������� ������ ��������� ���� ������ ������ �� ���������
		if (mode==STRICT_MODE && isVersionActual==UPDATE_NO) return isVersionActual;
			
		// �������� APK
		String updateSever = update[1]+"|"+update[2]+"|"+update[3]+"|"+update[4];
		String path = update[5];
		
		if (!connect(updateSever)) return UPDATE_ERROR;
		if (!downloadFile(Hnd,path,getExternalStorageDirectory().toString()+"/tmp/"+apkName)) return UPDATE_ERROR;
		
		File file = new File(getExternalStorageDirectory().toString()+"/tmp/"+apkName);
		mIntent = new Intent(Intent.ACTION_VIEW);
		mIntent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
		
		return UPDATE_YES;
	}
	
	/** �������� ��� ������� ���������� **/
	public Intent getIntentUpdate(){
		return mIntent;
	}
}
