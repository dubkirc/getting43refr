package com.example.get43hol2;

import static ru.dubki.base.Functions.piece;
import ru.dubki.base.DialogWait;
import ru.dubki.base.Msm;
import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;




public class MainActivity extends Activity {
	
	
	protected static final String View = null;
	//DialogWait mDialog;	
	DialogWait mDialog = new DialogWait(this);	 
	Handler h;
	int NRam,NVag;
	String MSMRet;
	
	public void onClickStart(View V){
					
		switch (V.getId())
		{
			case R.id.mbutton1:
				drawVacPack(false,false,"");
			break;
			case R.id.mbutton2:
				setContentView(R.layout.measure_first);
			break;
			case R.id.mbutton3:
				setContentView(R.layout.prer_fr);
				((Button) findViewById(R.id.Prep_Pack_Confirm)).setEnabled(false);
			break;
			case R.id.To_Main_Menu:
				setContentView(R.layout.fragment_main);
			break;
			
			case R.id.Beg_VP_Forward:
				setContentView(R.layout.vp_sec);
				((Button) findViewById(R.id.Sec_VP_Confirm)).setEnabled(false);
				((TextView) findViewById(R.id.vp_weight_brutto)).setText("300");
			break;
				
			case R.id.PrepPackScanYar:
				
			break;	
			
			case R.id.prep_Pack_Scan_Table:
				
			break;	
			
			case R.id.prep_pack_Mes:
				((TextView)findViewById(R.id.prep_pack_weight)).setText("300");//��������
			break;	
			
			case R.id.prep_pack_Abort:
				setContentView(R.layout.fragment_main);
			break;	
			
			case R.id.Prep_Pack_Confirm:
				
			break;
			
			case R.id.Mes_End_Abort:
				setContentView(R.layout.fragment_main);
			break;
			case R.id.Sec_VP_Confirm:
				new Thread(confirmVP).start();
			break;
			case R.id.Sec_VP_Measure:
				new Thread(weightFrameVP).start();
			break;
			
			}
	
	}
	
    /** ��������� ������ ���������� **/
	public void onClick(View v) {
	EditText NumRam = (EditText)findViewById(R.id.vp1_NRam);
	//NRam = Integer.parseInt((NumRam).getText().toString()); 	
	switch (v.getId()) {
		case R.id.btn_key_E:
			if (NumRam.toString().equals("")) return;
			int tempRam = 0;
			//try {
			tempRam=Integer.parseInt((NumRam).getText().toString());
			//	} 
		
			NRam=tempRam;
			new Thread(reqFrameData).start();
		break;
		case R.id.btn_key_clear:
			NumRam.setText("");
		break;	
		default:
			if (NumRam.getText().length() < 4)
			NumRam.setText(NumRam.getText()+v.getTag().toString());
		break;
	}
}
	
	public void onClick_mes(View v) {
		EditText mes_NRam = (EditText)findViewById(R.id.mes_Input);
		//NRam = Integer.parseInt((NumRam).getText().toString());
		switch (v.getId()) {
			case R.id.btn_key_E:
				if (mes_NRam.getText().toString().equals("")) return;
				if (findViewById(R.id.mes_PF_text_inNRAM1).isShown())
					{NRam=Integer.parseInt((mes_NRam).getText().toString());
					new Thread(reqMesFrameData).start();}
				if (findViewById(R.id.mes_PF_text_inVag1).isShown()&&Integer.parseInt(((TextView)findViewById(R.id.mes_Input)).getText().toString())>5)//
					{((TextView) findViewById(R.id.MesFirInfo)).setText("����� ������� �� ����� ���� ������ 5, ������� ���������� �����");
					((TextView) findViewById(R.id.MesFirInfo)).setTextColor(getResources().getColor(R.color.background_red));
					}
				if (findViewById(R.id.mes_PF_text_inVag1).isShown()&&Integer.parseInt(((TextView)findViewById(R.id.mes_Input)).getText().toString())<1)//
					{((TextView) findViewById(R.id.MesFirInfo)).setText("����� ������� ������ ���� ������ 0, ������� ���������� �����");
					((TextView) findViewById(R.id.MesFirInfo)).setTextColor(getResources().getColor(R.color.background_red));
					}	
				if (findViewById(R.id.mes_PF_text_inVag1).isShown()&&Integer.parseInt(((TextView)findViewById(R.id.mes_Input)).getText().toString())<6&&Integer.parseInt(((TextView)findViewById(R.id.mes_Input)).getText().toString())>0)	
					{NVag=Integer.parseInt(((TextView)findViewById(R.id.mes_Input)).getText().toString());
					setContentView(R.layout.measure_sec);
					((TextView)findViewById(R.id.temp_weight)).setText("0");
					((TextView)findViewById(R.id.switcher)).setText("1");
					}
				break;
			case R.id.btn_key_clear:
				mes_NRam.setText("");
			break;	
			case R.id.Mes_End_Mes:
				((TextView)findViewById(R.id.mes_brutto)).setText("300");//��������
				if (((TextView)findViewById(R.id.mes_addon)).getText().toString().equals(""))
					{}
				else
					{float a = Float.parseFloat(((TextView)findViewById(R.id.mes_brutto)).getText().toString());
					float b = Float.parseFloat(((TextView)findViewById(R.id.mes_addon)).getText().toString());
					float diff = a-b;
					((TextView)findViewById(R.id.mes_netto)).setText((Float.toString(diff)));
					((Button) findViewById(R.id.Mes_End_Confirm)).setEnabled(true);
					}
				((TextView)findViewById(R.id.Mes_End_Message)).setText("��������");
			break;	
			case R.id.Mes_End_Abort:
				setContentView(R.layout.fragment_main);	
			break;
			case R.id.Mes_End_Confirm:
			break;
			default:
				if (mes_NRam.getText().length() < 4)
				mes_NRam.setText(mes_NRam.getText()+v.getTag().toString());
			break;
		}
	}
	
	/**
	 * @param v
	 */
	public void onClick_mes2(View v) {
		int switcher = Integer.parseInt(((TextView)findViewById(R.id.switcher)).getText().toString());
	switch (v.getId()) {
		case R.id.Continue:
			switch (switcher){
			case 1:
				if(((TextView)findViewById(R.id.mes_sec_vag_weight)).getText().toString().equals(""))
					{((TextView)findViewById(R.id.MesSecInfo)).setText("���� �� ����� ���� ������! ������� ��� �������");
					((TextView) findViewById(R.id.MesSecInfo)).setTextColor(getResources().getColor(R.color.background_red));}
				else 
					{String x = (((TextView)findViewById(R.id.mes_sec_vag_weight)).getText().toString());
					((TextView)findViewById(R.id.switcher)).setText("2");
					((TextView)findViewById(R.id.MesSecInfo)).setText("������� ����� ������");
					((TextView) findViewById(R.id.MesSecInfo)).setTextColor(getResources().getColor(R.color.text_first));}
			break;
			case 2:
				if(((TextView)findViewById(R.id.mes_sec_shell_numb)).getText().toString().equals(""))
					{((TextView)findViewById(R.id.MesSecInfo)).setText("���� �� ����� ���� ������! ������� ����� ������");
					((TextView) findViewById(R.id.MesSecInfo)).setTextColor(getResources().getColor(R.color.background_red));}
				else 
					{((TextView)findViewById(R.id.switcher)).setText("3");
					((TextView)findViewById(R.id.MesSecInfo)).setText("������� ��� �����");
					((TextView) findViewById(R.id.MesSecInfo)).setTextColor(getResources().getColor(R.color.text_first));}
			break;
			case 3:
				if(((TextView)findViewById(R.id.mes_sec_shell_weight)).getText().toString().equals(""))
					{((TextView)findViewById(R.id.MesSecInfo)).setText("���� �� ����� ���� ������! ������� ��� �����");
					((TextView) findViewById(R.id.MesSecInfo)).setTextColor(getResources().getColor(R.color.background_red));}
				else 
					{float temp2 = Float.parseFloat(((TextView)findViewById(R.id.temp_weight)).getText().toString());
					temp2=temp2+Float.parseFloat(((TextView)findViewById(R.id.mes_sec_vag_weight)).getText().toString())+(Float.parseFloat(((TextView)findViewById(R.id.mes_sec_shell_numb)).getText().toString())*Float.parseFloat(((TextView)findViewById(R.id.mes_sec_shell_weight)).getText().toString()));
					((TextView)findViewById(R.id.temp_weight)).setText(Float.toString(temp2));
					((TextView)findViewById(R.id.switcher)).setText("1");
					((TextView)findViewById(R.id.mes_sec_vag_weight)).setText("");
					((TextView)findViewById(R.id.mes_sec_shell_numb)).setText("");
					((TextView)findViewById(R.id.mes_sec_shell_weight)).setText("");
					((TextView)findViewById(R.id.MesSecInfo)).setText("������� ��� �������");
					((TextView) findViewById(R.id.MesSecInfo)).setTextColor(getResources().getColor(R.color.text_first));
					int temp =Integer.parseInt(((TextView)findViewById(R.id.mes_Sec_vag_Num)).getText().toString());
					((TextView)findViewById(R.id.mes_Sec_vag_Num)).setText(Integer.toString(temp+1));
					if(NVag<Integer.parseInt(((TextView)findViewById(R.id.mes_Sec_vag_Num)).getText().toString()))
						{setContentView(R.layout.measure_end);
						((Button) findViewById(R.id.Mes_End_Confirm)).setEnabled(false);
						((TextView)findViewById(R.id.mes_addon)).setText(Float.toString(temp2));
						}					
					}
				break;
				default:
				break;}
		break;
		
		case R.id.btn_key_clear:
			switch (switcher){
			case 1:
				((TextView)findViewById(R.id.mes_sec_vag_weight)).setText("");
			break;
			case 2:
				((TextView)findViewById(R.id.mes_sec_shell_numb)).setText("");
			break;
			case 3:
				((TextView)findViewById(R.id.mes_sec_shell_weight)).setText("");
			break;}
		break;
		case R.id.mes_sec_back_to_menu:   
			setContentView(R.layout.fragment_main);
		break;		
		default:
			switch (switcher){
			case 1:
				((TextView)findViewById(R.id.mes_sec_vag_weight)).setText(((TextView)findViewById(R.id.mes_sec_vag_weight)).getText()+v.getTag().toString());
			break;
			case 2:
				((TextView)findViewById(R.id.mes_sec_shell_numb)).setText(((TextView)findViewById(R.id.mes_sec_shell_numb)).getText()+v.getTag().toString());
			break;
			case 3:
				((TextView)findViewById(R.id.mes_sec_shell_weight)).setText(((TextView)findViewById(R.id.mes_sec_shell_weight)).getText()+v.getTag().toString());
			break;}
		break;}
}
			
			
		Runnable reqFrameData = new Runnable() {
			@Override
			public void run() {
		 		mDialog.showDialogProgress("�����", "�������� ������...");
				
				Msm msm = new Msm();
				if(!msm.connect("192.168.1.16", "6567", "10", "DDD", "DDD")) {
					mDialog.showDialogBox("������", msm.getMessage());
					return;
				}
				
				if (!msm.run("ETAP1^DUVAKNZ2", NRam, null)) {
					mDialog.showDialogBox("������", msm.getMessage());
					return;
				}
				
				mDialog.hideDialog();
				
				String y = msm.outRet;
				
				Message msg;
				msg = h.obtainMessage(1,y);
				h.sendMessage(msg);

				
			}
		};

		Runnable weightFrameVP = new Runnable() {
			@Override
			public void run() {
		 		mDialog.showDialogProgress("�����", "�������� ������...");
				
				Msm msm = new Msm();
				if(!msm.connect("192.168.1.16", "6567", "10", "DDD", "DDD")) {
					mDialog.showDialogBox("������", msm.getMessage());
					return;
				}
				
				if (!msm.run("ETAP1C^DUVAKNZ2", NRam, piece((String)MSMRet,"\\|",8), Float.parseFloat(((TextView)findViewById(R.id.vp_weight_brutto)).getText().toString()), null)) {
					mDialog.showDialogBox("������", msm.getMessage());
					return;
				}
				
				mDialog.hideDialog();
				
				String y = msm.outRet;
				
				Message msg;
				msg = h.obtainMessage(3,y);
				h.sendMessage(msg);
			}
		};
		
		
		Runnable confirmVP = new Runnable() {
			@Override
			public void run() {
			
		 		mDialog.showDialogProgress("�����", "�������� ������...");
				
				Msm msm = new Msm();
				if(!msm.connect("192.168.1.16", "6567", "10", "DDD", "DDD")) {
					mDialog.showDialogBox("������", msm.getMessage());
					return;
				}
				
				if (!msm.run("ODN1^DUVAKNZ2", NRam, piece((String)MSMRet,"\\|",4), null)) {
					mDialog.showDialogBox("������", msm.getMessage());
					return;
				}
				
				mDialog.hideDialog();
			
				String y = msm.outRet;
				
				Message msg;
				msg = h.obtainMessage(4,y);
				h.sendMessage(msg);
			}
		};
		
		
		Runnable reqMesFrameData = new Runnable() {
			@Override
			public void run() {
		 		mDialog.showDialogProgress("�����", "�������� ������...");
				
				Msm msm = new Msm();
				
				if(!msm.connect("192.168.1.16", "6567", "10", "DDD", "DDD")) {
					mDialog.showDialogBox("������", msm.getMessage());
					return;
				}
				
				if (!msm.run("^HLADSPL9", NRam, null)) {
					mDialog.showDialogBox("������", msm.getMessage());
					return;
				}
				
				mDialog.hideDialog();
				
				String y = msm.outRet;
		
				Message msg;
				msg = h.obtainMessage(2,y);
				h.sendMessage(msg);
			}
		};

	
/*private Runnable test = new Runnable() {
	
	@Override
	public void run() {
		
	}
};*/



/*
	Handler handler = new Handler(new Callback() {
		
		@Override
		public boolean handleMessage(Message arg0) {
			String y = (String)arg0.obj;
			return false;
		}
	});
	*/
	

	
	public void drawVacPack(boolean x,boolean y, String z)
	{
		setContentView(R.layout.vp_first);
		((Button) findViewById(R.id.Beg_VP_Forward)).setEnabled(false);
//		((EditText) findViewById(R.id.vpMLNum)).setEnabled(false);;
		//((TextView) findViewById(R.id.vpFrameNum)).setEnabled(false);;
//		((EditText) findViewById(R.id.diffweight)).setEnabled(false);
//		((EditText) findViewById(R.id.mesweight)).setEnabled(false);
//		EditText VPMessage = (EditText) findViewById(R.id.VPMessage);
//		VPMessage.setText("   ");
//		((Button) findViewById(R.id.vpbutton2)).setEnabled(x);
//		((Button) findViewById(R.id.vpbutton3)).setEnabled(y);
				
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.fragment_main);

		h = new Handler() {
		    public void handleMessage(android.os.Message msg) {
		        int i = msg.what;
		    	switch(i){
		    	case 1:
		    		EditText VPInNumFr=(EditText)findViewById(R.id.MesInNumFr);
		     		if (piece((String)msg.obj,"\\|",1).equals("+"))
		     		{((Button) findViewById(R.id.Beg_VP_Forward)).setEnabled(true);
		     		VPInNumFr.setText("����������� ���������    "+piece((String)msg.obj,"\\|",9));
		     		MSMRet=(String)msg.obj;
		     		VPInNumFr.setTextColor(getResources().getColor(R.color.text_first));
		     		}
		     		else
		     		{((Button) findViewById(R.id.Beg_VP_Forward)).setEnabled(false);
		     		VPInNumFr.setText(piece((String)msg.obj,"\\|",2));
		     		VPInNumFr.setTextColor(getResources().getColor(R.color.background_red));}
		    	break;
		    	case 2:
		    		EditText mes_NRam=(EditText)findViewById(R.id.mes_Input);
		    		if (piece((String)msg.obj,"\\|",1).equals("+")&&piece((String)msg.obj,"\\|",5).equals("1"))
		     			{//((Button) findViewById(R.id.Mes_Forward)).setEnabled(true);
		    			((TextView)findViewById(R.id.mes_PF_text_inNRAM1)).setVisibility(TextView.INVISIBLE);
		    			((TextView)findViewById(R.id.mes_PF_text_inNRAM2)).setVisibility(TextView.INVISIBLE);
		    			((TextView)findViewById(R.id.mes_PF_text_inVag1)).setVisibility(TextView.VISIBLE);
		    			((TextView)findViewById(R.id.mes_PF_text_inVag2)).setVisibility(TextView.VISIBLE);
		    			((EditText)findViewById(R.id.MesFirInfo)).setText("�������: "+piece((String)msg.obj,"\\|",2)+".  ��:"+piece((String)msg.obj,"\\|",3)+piece((String)msg.obj,"\\|",4)); ///  ����������� ���������
		    			((EditText)findViewById(R.id.mes_Input)).setText("");
		    			MSMRet=(String)msg.obj;
		    			((TextView) findViewById(R.id.MesFirInfo)).setTextColor(getResources().getColor(R.color.text_first));}
		     		if	(piece((String)msg.obj,"\\|",1).equals("+")&&piece((String)msg.obj,"\\|",5).equals("0"))
		     			{
		     			setContentView(R.layout.measure_end);
		     			((TextView)findViewById(R.id.TextView01)).setVisibility(TextView.INVISIBLE);
		    			((TextView)findViewById(R.id.TextView02)).setVisibility(TextView.INVISIBLE);
		    			((TextView)findViewById(R.id.mes_netto)).setVisibility(TextView.INVISIBLE);
		    			((TextView)findViewById(R.id.mes_addon)).setVisibility(TextView.INVISIBLE);
		    			((TextView) findViewById(R.id.Mes_End_Message)).setTextColor(getResources().getColor(R.color.text_first));
		     			}
		     		if (!(piece((String)msg.obj,"\\|",1).equals("+")))
		     			{((EditText)findViewById(R.id.MesFirInfo)).setText(piece((String)msg.obj,"\\|",2));
		     			((TextView) findViewById(R.id.MesFirInfo)).setTextColor(getResources().getColor(R.color.red));}
		     	break;
		    	case 3:
		    		MSMRet=(String)msg.obj;
		    		if (piece(MSMRet,"\\|",1).equals("+"))
		     		{((Button) findViewById(R.id.Sec_VP_Confirm)).setEnabled(true);
		     		((TextView) findViewById(R.id.MesInNumFr)).setText("������ ������ ���������  ");
		     		((TextView) findViewById(R.id.vp_frame_sticks)).setText((piece(MSMRet,"\\|",3)));
		     		((TextView) findViewById(R.id.vp_weight_netto)).setText((piece(MSMRet,"\\|",2)));
		     		((TextView) findViewById(R.id.MesInNumFr)).setTextColor(getResources().getColor(R.color.text_first));}
		     		else
		     		{((Button) findViewById(R.id.Sec_VP_Confirm)).setEnabled(false);
		     		((TextView) findViewById(R.id.MesInNumFr)).setText(piece((String)msg.obj,"\\|",2));
		     		((TextView) findViewById(R.id.MesInNumFr)).setTextColor(getResources().getColor(R.color.background_red));
		     		;}
		    	break;
		    	case 4:
		    		MSMRet=(String)msg.obj;
		    		if (piece(MSMRet,"\\|",1).equals("+"))
		     		{((Button) findViewById(R.id.Sec_VP_Confirm)).setEnabled(false);
		     		((TextView) findViewById(R.id.vp_weight_err)).setText(piece((String)msg.obj,"\\|",2));
		     		((TextView) findViewById(R.id.MesInNumFr)).setText("�������  ");
		     		((TextView) findViewById(R.id.MesInNumFr)).setTextColor(getResources().getColor(R.color.text_first))
		     		;}
		       		else
		     		{((TextView) findViewById(R.id.MesInNumFr)).setText(piece((String)msg.obj,"\\|",2));
		     		((TextView) findViewById(R.id.MesInNumFr)).setTextColor(getResources().getColor(R.color.background_red));
		     		}
		    	break;
		    	
		    	}
		    };
		};	
		
		}
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */


}
